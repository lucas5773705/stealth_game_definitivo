using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    private int m_HP = 100;

    private Rigidbody[] m_Bones;

    [SerializeField]
    private GameEvent m_GameEvent;

    private void Awake()
    {
        //m_Animator = GetComponent<Animator>();
        m_Bones = GetComponentsInChildren<Rigidbody>();
        Activate(false);

        foreach (Rigidbody bone in m_Bones)
            bone.GetComponent<IDamageable>().OnDamage += ReceiveDamage;
    }

    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
        //m_Animator.enabled = !state;
    }

    private void ReceiveDamage(int damage)
    {
        m_HP -= damage;
        if (m_HP <= 0)
            Die();
        m_GameEvent.Raise();
    }

    private void Die()
    {
        foreach (Rigidbody bone in m_Bones)
            bone.GetComponent<IDamageable>().OnDamage -= ReceiveDamage;

        Activate(true);
    }
}
