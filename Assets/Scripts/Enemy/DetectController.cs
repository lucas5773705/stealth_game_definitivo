using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectController : MonoBehaviour
{
    [SerializeField]
    GameEvent gameEvent; 
    [SerializeField]
    GameEventVector3 gameEventVector3;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameEventVector3.Raise(other.transform.position);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            gameEventVector3.Raise(other.transform.position);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        gameEvent.Raise();
    }
}
