using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class EnemyController : MonoBehaviour
{

    private bool enemyOnTrigger = false;
    private enum SwitchMachineStates { NONE, IDLE, WALK, HIT1, DEAD, FOLLOW, WAITING };
    private SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        m_Agent.ResetPath();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                //m_Rigidbody.velocity = Vector3.zero;
                if (!seguir)
                    ChangeState(SwitchMachineStates.WALK);
                if (seguir)
                    ChangeState(SwitchMachineStates.FOLLOW);
                break;

            case SwitchMachineStates.WALK:
                Patrullar();

                break;

            case SwitchMachineStates.HIT1:
                if (!cooldown)
                {
                    Shoot();
                    StartCoroutine(Cooldown());
                    break;
                }

                break;

            case SwitchMachineStates.DEAD:
                seguir = false;
                attack = false;
                alive = false;
                break;

            case SwitchMachineStates.FOLLOW:
                Seguir(jugador);
                break;

            case SwitchMachineStates.WAITING:
                seguir = false;
                waiting = true;
                StartCoroutine(WaitingTime());
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:

                break;

            case SwitchMachineStates.DEAD:

                break;

            case SwitchMachineStates.FOLLOW:

                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {

        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (seguir)
                    ChangeState(SwitchMachineStates.FOLLOW);

                if (attack)
                    ChangeState(SwitchMachineStates.HIT1);
                break;
            case SwitchMachineStates.WALK:
                Patrullar();
                if (attack)
                    ChangeState(SwitchMachineStates.HIT1);

                if (seguir)
                    ChangeState(SwitchMachineStates.FOLLOW);
                break;
            case SwitchMachineStates.HIT1:
                if (!attack)
                {
                    if (seguir)
                    {
                        ChangeState(SwitchMachineStates.FOLLOW);
                    }
                    else
                    {
                        ChangeState(SwitchMachineStates.IDLE);
                    }
                }

                break;

            case SwitchMachineStates.DEAD:

                break;

            case SwitchMachineStates.FOLLOW:
                Seguir(jugador);
                if (attack)
                {
                    ChangeState(SwitchMachineStates.HIT1);
                }
                break;
            default:
                break;
        }
    }
    private SphereCollider esfera;
    private NavMeshAgent m_Agent;
    private Rigidbody m_Rigidbody;
    public bool cooldown = false;
    [Header("Enemy Values")]

    public bool seguir;
    public bool attack;
    [SerializeField]
    private LayerMask m_ShootMask;
    private bool alive = true;
    private bool waiting = false;
    void Awake()
    {
        alive = true;
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Agent = GetComponent<NavMeshAgent>();
        esfera = GetComponent<SphereCollider>();
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }
    void Update()
    {
        UpdateState();
    }

    private Transform jugador;
    public void Seguir(Transform jugador)
    {
        if (seguir && !attack)
        {
            if (!m_Agent.pathPending)
                m_Agent.SetDestination(jugador.position);
            Debug.Log(jugador);
            m_Agent.stoppingDistance = 5;
            Debug.Log("m_Agent.velocity: " + m_Agent.velocity);
            if (m_Agent.velocity == Vector3.zero)
            {
                attack = true;
                Debug.Log("ATTACK");
                ChangeState(SwitchMachineStates.HIT1);
            }
        }
    }

    [SerializeField]
    private Transform pos1;
    [SerializeField]
    private Transform pos2;
    [SerializeField]
    private bool llegado = false;
    private void Patrullar()
    {
        if (!seguir && !waiting)
        {
            Vector3 destination = llegado ? pos2.position : pos1.position;

            if (!m_Agent.pathPending)
                m_Agent.SetDestination(destination);

            float distanciaUmbral = 0.3f;
            //Debug.Log("Distancia al destino: " + Vector3.Distance(transform.position, destination));
            if (Vector3.Distance(transform.position, destination) < distanciaUmbral)
            {
                llegado = !llegado;
            }
        }
    }

    private void Shoot()
    {
        float x = Random.Range(-1f, 1f);
        float y = Random.Range(-1f, 1f);

        Vector3 direction = transform.forward + new Vector3(x, y, 0);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, 20f, m_ShootMask))
        {
            Debug.DrawLine(transform.position, hit.point, Color.red, 2f);

            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
            {
                target.Damage(20);
            }
        }
        cooldown = true;
        attack = false;
    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(1);
        cooldown = false;
    }

    IEnumerator WaitingTime()
    {
        yield return new WaitForSeconds(10);
        seguir = false;
        waiting = false;
        ChangeState(SwitchMachineStates.WALK);
    }

    private void Wait()
    {
        ChangeState(SwitchMachineStates.WAITING);
    }

    float anguloVision = -110f;
    private void OnTriggerStay(Collider other)
    {
        if (alive)
        {
            if (other.tag == "Player")
            {
                Vector3 direccion = other.transform.position - transform.position;

                float angulo = Vector3.Angle(direccion, transform.forward);

                if (-angulo > anguloVision * 0.5f)
                {
                    RaycastHit hit;

                    if (Physics.Raycast(transform.position + transform.up, direccion.normalized, out hit))
                    {
                        Debug.DrawRay(transform.position + transform.up, direccion);
                        if (hit.collider.tag == "Player")
                        {
                            enemyOnTrigger = true;
                            seguir = true;
                            jugador = hit.transform;
                        }
                    }
                }
                else
                {
                    if (enemyOnTrigger)
                    {
                        seguir = false;
                        attack = false;
                        enemyOnTrigger = false;
                        Wait();
                    }

                }
                
            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enemyOnTrigger = true; 
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (alive && seguir) 
        {
            seguir = false;
            attack = false;
            Wait(); 
        }

    }

    public void Dead()
    {
        ChangeState(SwitchMachineStates.DEAD);
    }
}
