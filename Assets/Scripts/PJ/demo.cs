using System;
using System.Collections;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.InputSystem;
using Input = UnityEngine.Input;

public class demo : MonoBehaviour
{

    private bool isRotating = false;
    private float rotationTimer = 0f;
    private float rotationDuration = 1.5f;
    private int m_HP = 20;

    [SerializeField]
    private InputActionAsset m_Input;
    private InputAction m_movement;

    [SerializeField]
    private float m_Speed = 3f;

    [SerializeField]
    private float m_RotationSpeed = 180f;

    Vector3 m_Movement = Vector3.zero;
    private Rigidbody m_Rigidbody;

    public float rotationSpeed = 100f;


    //ANIMATOR
    [SerializeField]
    private Animator m_Animator;

    //CUERPO
    [SerializeField]
    private GameObject spine;

    //BONES
    [SerializeField]
    private Transform Bones;
    private Rigidbody[] m_Bones;



    //STATES BOOLEANS
    private Boolean walking = false;
    private Boolean running = false;
    private Boolean aiming = false;
    private Boolean reload = false;
    private int Health;
    private int maxHealth;
    private int jumpForce = 100;


    //PIVOTE
    [SerializeField]
    private Transform camara;

    //Defuse
    [SerializeField]
    private GameEvent onDefuse;

    [SerializeField]
    private GameEvent onCancelDefuse;

    [SerializeField]
    LayerMask layer;

    private enum SwitchMachineStates { NONE, IDLE, WALK, SHOT, RUNNING, AIMING, CANCEL_AIMING, HIT2, RELOAD_RIFLE, RELOAD_RIFLE_IDLE, DEAD, CROUCH, DEFUSE };
    [SerializeField]
    private SwitchMachineStates m_CurrentState;

    //canviarem d'estat sempre mitjanant aquesta funci
    private void ChangeState(SwitchMachineStates newState)
    {
        //no caldria fer-ho, per evitem que un estat entri a si mateix
        //s possible que la nostra mquina ho permeti, per tant aix
        //no es faria sempre.
        if (newState == m_CurrentState)
            return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;

        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector3.zero;
                walking = false;
                running = false;
                m_Animator.CrossFade("idle", 0.1f);
                //m_Animator.CrossFade("idle_aiming", 0.1f);
                break;


            case SwitchMachineStates.WALK:

                walking = true;
                m_Animator.CrossFade("walk", 0.1f);
                //m_Animator.CrossFade("walk",.1f);

                break;

            case SwitchMachineStates.RUNNING:
                running = true;
                m_Animator.CrossFade("run", .2f);

                break;



            case SwitchMachineStates.SHOT:



                break;

            case SwitchMachineStates.HIT2:

                m_Rigidbody.velocity = Vector3.zero;
                m_Animator.Play("hit2");

                break;

            case SwitchMachineStates.CROUCH:

                m_Rigidbody.velocity = Vector3.zero;
                m_Animator.CrossFade("crouch", .1f);

                break;
            case SwitchMachineStates.DEFUSE:

                m_Rigidbody.velocity = Vector3.zero;
                m_Animator.CrossFade("idle", 0.1f);
                Desactivar();
                break;



            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.SHOT:


                break;

            case SwitchMachineStates.HIT2:


                break;
            case SwitchMachineStates.DEFUSE:


                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector3.zero;



                break;
            case SwitchMachineStates.WALK:

                FaceForward();
                m_Movement = m_movement.ReadValue<Vector3>() * m_Speed;
                m_Rigidbody.velocity = Vector3.ProjectOnPlane(camara.forward, transform.up) * m_Movement.z
                                     + camara.right * m_Movement.x;
                if (m_Rigidbody.velocity == Vector3.zero)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }

                if (m_Movement.z < 0)
                {
                    m_Animator.CrossFade("left_turn", .2f);
                }
                break;

            case SwitchMachineStates.RUNNING:
                FaceForward();
                m_Movement = m_movement.ReadValue<Vector3>() * m_Speed * 2;
                m_Rigidbody.velocity = Vector3.ProjectOnPlane(camara.forward, transform.up) * m_Movement.z
                                   + camara.right * m_Movement.x;

                break;



            case SwitchMachineStates.HIT2:

                break;
            case SwitchMachineStates.DEFUSE:
                Desactivar();
                break;

            default:
                break;
        }
    }
    [SerializeField]
    private bool onRange;
    public void OnRange()
    {
        onRange = !onRange;
    }

    private void FaceForward()
    {
        transform.forward = m_Rigidbody.velocity;
    }



    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();
        m_Bones = Bones.GetComponentsInChildren<Rigidbody>();
        Activate(false);

        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage += ReceiveDamage;

    }

    void Start()
    {
        onRange = false;
        m_Speed = 3f;
        maxHealth = 10;
        Health = maxHealth;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        m_Input.FindActionMap("Movement").FindAction("Move").started += OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").performed += OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").canceled += OnCancelMove;
        m_Input.FindActionMap("Movement").FindAction("Shoot").performed += Shoot;
        m_Input.FindActionMap("Movement").FindAction("Shoot").canceled += OnCancelShot;
        m_Input.FindActionMap("Movement").FindAction("Running").performed += OnRun;
        m_Input.FindActionMap("Movement").FindAction("Running").canceled += OnCancelRun;
        m_Input.FindActionMap("Movement").FindAction("Crouch").performed += OnCrouch;
        m_Input.FindActionMap("Movement").FindAction("Crouch").canceled += OnCancelCrouch;
        m_Input.FindActionMap("Movement").FindAction("Aim").performed += OnAim;
        m_Input.FindActionMap("Movement").FindAction("Slide").performed += OnSlide;
        m_Input.FindActionMap("Movement").FindAction("Jump").performed += OnJump;
        m_Input.FindActionMap("Movement").FindAction("Rapidfire").performed += OnRapidfire;
        m_Input.FindActionMap("Movement").FindAction("Aim").canceled += OnCancelAim;
        m_Input.FindActionMap("Movement").FindAction("Rifle_Reload").performed += OnRifleReload;
        m_Input.FindActionMap("Movement").FindAction("Defuse").canceled += OnCancelDefuse;
        m_Input.FindActionMap("Movement").FindAction("Defuse").performed += OnDefuse;
        m_movement = m_Input.FindActionMap("Movement").FindAction("Move");
        m_Input.FindActionMap("Movement").Enable();

        InitState(SwitchMachineStates.IDLE);


    }
    private void FixedUpdate()
    {

    }



    void Update()
    {

        UpdateState();
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Die();

        }

        UpdateState();
        if (Input.GetKeyDown(KeyCode.E))
        {
            Activate(false);

        }
    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Movement").FindAction("Move").started -= OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").performed -= OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").canceled -= OnCancelMove;
        m_Input.FindActionMap("Movement").FindAction("Shoot").performed -= Shoot;
        m_Input.FindActionMap("Movement").FindAction("Shoot").canceled -= OnCancelShot;
        m_Input.FindActionMap("Movement").FindAction("Running").performed -= OnRun;
        m_Input.FindActionMap("Movement").FindAction("Running").canceled -= OnCancelRun;
        m_Input.FindActionMap("Movement").FindAction("Crouch").performed -= OnCrouch;
        m_Input.FindActionMap("Movement").FindAction("Crouch").canceled -= OnCancelCrouch;
        m_Input.FindActionMap("Movement").FindAction("Aim").performed -= OnAim;
        m_Input.FindActionMap("Movement").FindAction("Slide").performed -= OnSlide;
        m_Input.FindActionMap("Movement").FindAction("Jump").performed -= OnJump;
        m_Input.FindActionMap("Movement").FindAction("Rapidfire").performed -= OnRapidfire;
        m_Input.FindActionMap("Movement").FindAction("Aim").canceled -= OnCancelAim;
        m_Input.FindActionMap("Movement").FindAction("Rifle_Reload").performed -= OnRifleReload;
        m_Input.FindActionMap("Movement").FindAction("Defuse").canceled -= OnCancelDefuse;
        m_Input.FindActionMap("Movement").FindAction("Defuse").performed -= OnDefuse;
        m_Input.FindActionMap("Movement").Disable();
    }
    private void Desactivar()
    {
        if (onRange)
            onDefuse.Raise();
        /*
        RaycastHit hit;
        if (Physics.Raycast(camara.transform.position, transform.forward, out hit, 5f, layer))
        {
            Debug.DrawLine(transform.position, hit.point, Color.green, 5f);
            if (hit.transform.tag == "bomb")
            {
                onDefuse.Raise();
            }
        }*/
    }
    public void OnRun(InputAction.CallbackContext context)
    {

        if (walking)
        {
            m_Movement = m_movement.ReadValue<Vector3>() * m_Speed;
            if (m_Movement.x < 0)
            {

                StartCoroutine(RotatePlayer(-90f));
            }
            else if (m_Movement.x > 0)
            {

                StartCoroutine(RotatePlayer(90f));
            }
            ChangeState(SwitchMachineStates.RUNNING);

        }
    }

    public void OnDefuse(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.DEFUSE);
    }
    public void OnCancelDefuse(InputAction.CallbackContext context)
    {
        onCancelDefuse.Raise();
        ChangeState(SwitchMachineStates.IDLE);
    }
    public void OnCrouch(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.CROUCH);
    }

    public void OnCancelCrouch(InputAction.CallbackContext context)
    {

    }

    public void OnJump(InputAction.CallbackContext context)
    {
        m_Rigidbody.AddForce(Vector3.up * jumpForce);
    }

    public void OnRapidfire(InputAction.CallbackContext context)
    {

    }


    public void OnAim(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.AIMING);

    }

    public void OnRifleReload(InputAction.CallbackContext context)
    {
        if (walking)
            ChangeState(SwitchMachineStates.RELOAD_RIFLE);
        else
            ChangeState(SwitchMachineStates.RELOAD_RIFLE_IDLE);


    }

    public void OnCancelAim(InputAction.CallbackContext context)
    {

        ChangeState(SwitchMachineStates.CANCEL_AIMING);

    }

    public void OnCancelShot(InputAction.CallbackContext context)
    {
        print("eooo");
        //canShoot = false;
        //ChangeState(SwitchMachineStates.CANCEL_AIMING);

    }

    public void OnSlide(InputAction.CallbackContext context)
    {
        if (running)
        {
            m_Rigidbody.velocity = m_Movement = m_movement.ReadValue<Vector3>() * m_Speed * 3;
            m_Animator.CrossFade("slide", 0.02f);
        }

    }


    public void OnMove(InputAction.CallbackContext context)
    {
        m_Movement = m_movement.ReadValue<Vector3>() * m_Speed;
        if (!running && !walking)
        {
            if (m_Movement.x < 0)
            {
                m_Animator.CrossFade("turn_left", 0.1f);
                //StartCoroutine(RotatePlayer(-90f));
            }
            else if (m_Movement.x > 0)
            {
                m_Animator.CrossFade("turn_right", 0.1f);
                //StartCoroutine(RotatePlayer(90f));
            }
        }

        if (walking && !running)
        {
            if (m_Movement.x < 0)
            {

                //StartCoroutine(RotatePlayer(-90f));
            }
            else if (m_Movement.x > 0)
            {

                //StartCoroutine(RotatePlayer(90f));
            }
        }

        if (running)
        {
            if (m_Movement.x < 0)
            {

                //StartCoroutine(RotatePlayer(-90f));
            }
            else if (m_Movement.x > 0)
            {

                //StartCoroutine(RotatePlayer(90f));
            }
        }

        //if(m_Movement.z > 0 && !running)
        print(m_Movement);
        ChangeState(SwitchMachineStates.WALK);
    }

    public void OnCancelMove(InputAction.CallbackContext context)
    {

        ChangeState(SwitchMachineStates.IDLE);

    }


    public void OnCancelRun(InputAction.CallbackContext context)
    {
        running = false;
        //ChangeState(SwitchMachineStates.IDLE);

    }
    private void Shoot(InputAction.CallbackContext context)
    {

        ChangeState(SwitchMachineStates.SHOT);
    }



    private IEnumerator RotatePlayer(float targetAngle)
    {
        isRotating = true;

        float totalRotation = 0f;
        m_Movement = m_movement.ReadValue<Vector3>() * m_Speed;
        while (m_Movement.x < 0 || m_Movement.x > 0)
        {

            if (m_Movement.x < 0)
            {
                float rotationAmount = -rotationSpeed * Time.deltaTime;
                transform.Rotate(Vector3.up * rotationAmount);
                totalRotation += Mathf.Abs(rotationAmount);
            }
            else
            {
                float rotationAmount = rotationSpeed * Time.deltaTime;
                transform.Rotate(Vector3.up * rotationAmount);
                totalRotation += Mathf.Abs(rotationAmount);
            }


            yield return null;
        }

        // Restablecer la marca de rotaci�n
        isRotating = false;
    }

    public void Stab1()
    {
        m_Animator.CrossFade("stab1", 0.02f);
    }


    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
        m_Animator.enabled = !state;
    }

    public void Activate2()
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = true;
        m_Animator.enabled = true;
    }

    private void ReceiveDamage(int damage)
    {


        m_HP -= damage;
        //ChangeState(SwitchMachineStates.HIT);

    }

    private void Die()
    {
        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage -= ReceiveDamage;



        Activate(true);
    }

    public void Damage(int amount)
    {
        throw new NotImplementedException();
    }



}
