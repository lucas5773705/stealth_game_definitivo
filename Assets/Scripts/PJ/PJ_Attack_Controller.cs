using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PJ_Attack_Controller : MonoBehaviour
{
    [SerializeField]
    private float attackRadius;

    [SerializeField]
    private float maxDistance;

    [SerializeField]
    private LayerMask enemyMask;

    public float umbral = 90f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Attack();
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position - transform.up * maxDistance, attackRadius);
        

    }


       private void Attack()
    {
       

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, attackRadius, enemyMask))
        {
            
            GameObject enemy = hit.collider.gameObject;
            if (enemy != null)
            {
                print("entra bien");
                DetectBackstab(enemy);
            }
        }
    }

    private void DetectBackstab(GameObject enemy)
    {
        /*
        Vector3 directionToEnemy = enemy.transform.position - transform.position;
        Vector3 enemyBackward = -enemy.transform.forward;

        float angle = Vector3.Angle(directionToEnemy.normalized, enemyBackward.normalized);
        */
        //Vector3 direccion = enemy.transform.position - transform.position;

        float angulo = Vector3.Angle(enemy.transform.forward, transform.forward);

        Debug.Log("angulo: " + angulo);

        if (angulo < umbral)
        {
            print("hola");
            gameObject.GetComponent<PJ_Controller>().Stab1();
        }

        /*
        if (angle < umbral)
        {
            // Realizar acciones específicas de "acuchillar por la espalda"
            gameObject.GetComponent<PJ_Controller>().Stab1(); 
            //enemy.GetComponent<Enemy_Controller>().Die();
        }
        */
    }
}
