using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Ranking : ScriptableObject
{
    public List<float> ranks;
    public float actualRank;

}
