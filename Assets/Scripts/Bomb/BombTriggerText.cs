using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombTriggerText : MonoBehaviour
{
    [SerializeField]
    GameEventString gameEventUI;

    [SerializeField]
    GameEvent gameEventCircle;

    [SerializeField]
    GameEvent pjOnRange;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameEventUI.Raise("Hold [E] to defuse the bomb");
            gameEventCircle.Raise();
            pjOnRange.Raise();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            gameEventUI.Raise("");
            gameEventCircle.Raise();
            pjOnRange.Raise();
        }
    }
}
