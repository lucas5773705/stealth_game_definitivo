using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField]
    private float tempsRestant;
    [SerializeField]
    private float tempsDesactivar;

    [SerializeField]
    GameEventFloat pintar;
    private bool fin;

    [SerializeField]
    GameEvent endGame;

    void Start()
    {
        tempsRestant = tempsDesactivar;
        fin = false;
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void Desactivant()
    {
        if (tempsRestant > 0)
            tempsRestant -= Time.deltaTime;
        else
        {
            tempsRestant = 0;
            fin = true;
            endGame.Raise();
            //guardar tiempo y cambiar escena
        }
        pintar.Raise(tempsRestant/tempsDesactivar);

    }

    public void Cancel()
    {
        if (!fin)
            tempsRestant = tempsDesactivar;
        pintar.Raise(tempsRestant / tempsDesactivar);
    }


}
