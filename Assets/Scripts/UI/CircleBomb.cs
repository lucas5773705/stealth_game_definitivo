using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleBomb : MonoBehaviour
{
    [SerializeField] 
    private Image circle;

    public void Pintar(float percent)
    {
        circle.fillAmount = percent/1;
    }
}
