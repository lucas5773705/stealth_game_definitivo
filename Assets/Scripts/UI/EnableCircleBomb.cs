using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableCircleBomb : MonoBehaviour
{
    [SerializeField]
    GameObject circle;
    public void Activate()
    {
        circle.SetActive(!circle.activeSelf);
    }
}
