using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DefuseText : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI textMeshProUGUI;
    public void ShowText(string text)
    {
        textMeshProUGUI.text = text;
    }
}
