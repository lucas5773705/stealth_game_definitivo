using TMPro;
using UnityEngine;

public class UIRanking : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timeText;

    [SerializeField]
    Ranking ranking;

    private void Start()
    {
        timeText.text = "Ranking" + "\n";
        MostrarRanking();
    }
    private void MostrarRanking()
    {
        float minuts;
        float segons;
        float milisegons;
        if (ranking.ranks.Count >= 5)
        {
            ranking.ranks.Add(ranking.actualRank);
            ranking.ranks.Sort();
            //ranking.ranks.Reverse();
            while (ranking.ranks.Count > 5)
            ranking.ranks.RemoveAt(ranking.ranks.Count - 1);
        }
        else
        {
            ranking.ranks.Add(ranking.actualRank);
            ranking.ranks.Sort();
            //ranking.ranks.Reverse();
        }

        foreach (float rank in ranking.ranks)
        {
            minuts = Mathf.FloorToInt(rank / 60);
            segons = Mathf.FloorToInt(rank % 60);
            milisegons = rank % 1 * 1000;
            timeText.text += string.Format("{0:00}:{1:00}:{2:00}" + "\n", minuts, segons, milisegons);
        }
        timeText.text += "\n" + "\n";
        minuts = Mathf.FloorToInt(ranking.actualRank / 60);
        segons = Mathf.FloorToInt(ranking.actualRank % 60);
        milisegons = ranking.actualRank % 1 * 1000;
        timeText.text += string.Format("Actual: {0:00}:{1:00}:{2:00}" + "\n", minuts, segons, milisegons); ;
    }
}
