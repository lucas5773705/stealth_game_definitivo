using TMPro;
using UnityEngine;

public class TimeUpdate : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timeText;
    public void MostrarTemps(float temps)
    {
        float minuts = Mathf.FloorToInt(temps / 60);
        float segons = Mathf.FloorToInt(temps % 60);
        if (temps <= 0)
            timeText.text = string.Format("00:00:00");
        else if (temps <= 10)
        {
            float milisegons = temps % 1 * 1000;
            timeText.text = string.Format("{0:00}:{1:00}:{2:00}", minuts, segons, milisegons);
        }
        else
            timeText.text = string.Format("{0:00}:{1:00}", minuts, segons);
    }
}
