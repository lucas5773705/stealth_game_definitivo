using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour
{
    [SerializeField]
    private Ranking ranking;

    [SerializeField]
    private float tiempoLimite;

    private float tiempoRestante;

    [SerializeField]
    private GameEventFloat gameEventFloat;

    private bool juegoTerminado;

    private void Start()
    {
        tiempoRestante = tiempoLimite;
    }

    void Update()
    {
        if (!juegoTerminado && tiempoRestante > 0)
        {
            tiempoRestante -= Time.deltaTime;
            gameEventFloat.Raise(tiempoRestante);

            if (tiempoRestante <= 0)
            {
                Debug.Log("�Se acab� el tiempo!");
                tiempoRestante = 0;
                EndGame();
            }
        }
    }

    private void EndGame()
    {
        if (!juegoTerminado)
        {
            Debug.Log("Fin del juego");
            float tiempo = tiempoLimite - tiempoRestante;
            ranking.actualRank = tiempo;
            juegoTerminado = true;
            SceneManager.LoadScene("GameOver");
        }
    }
}