using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class HitboxController : MonoBehaviour, IDamageable, IPushable
    {
        public event Action<int> OnDamage;

        public void Damage(int amount)
        {
            Debug.Log($"{gameObject} impactat per un valor de {amount} punts de mal.");
            OnDamage?.Invoke(amount);
        }

        public void Push(Vector3 direction, float impulse)
        {
            GetComponent<Rigidbody>().AddForce(direction * impulse, ForceMode.Impulse);
        }
    }


