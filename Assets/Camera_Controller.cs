using UnityEngine;

public class Camera_Controller : MonoBehaviour
{

    [SerializeField]
    private GameObject PJ;

    [SerializeField]
    private bool m_InvertY = false;

    [SerializeField]
    private float m_CameraDistance = 1f;


    public float rotationSpeed = 100f;
    public float mouseSensitivity = 2f;
    public float maxVerticalAngle = 60f;
    public float minVerticalAngle = -60f;
    float mouseX = 0f;
    float mouseY = 0f;


    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed * mouseSensitivity * Time.deltaTime;
        mouseY += Input.GetAxis("Mouse Y") * -1 * rotationSpeed * mouseSensitivity * Time.deltaTime;
        mouseY = Mathf.Clamp(mouseY, minVerticalAngle, maxVerticalAngle);
    }

    private void LateUpdate()
    {
        transform.localEulerAngles = new Vector3(mouseY, mouseX, 0f);
        //transform.position = PJ.transform.position - transform.forward * m_CameraDistance;
        float x = -0.5f;
        transform.position = PJ.transform.position - (transform.forward * m_CameraDistance + new Vector3(x,0,0));

        //raycast PJ.transform.position - transform.forward * m_CameraDistance si toca posicion tocada menos un poquito

        //transform.position = direction * m_CameraDistance;


    }

    /*
    private void MoveCamera()
    {
        float mouseSensitivity = 300f;
        

        

        //float limitedX = Mathf.Clamp(mouseX, -1000f, 1000f);
        //transform.parent.localEulerAngles = new Vector3(0f, limitedX, 0f);
    }
    */


}
