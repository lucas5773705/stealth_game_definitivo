using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection_Controller : MonoBehaviour
{

    float anguloVision = -110f;

    private bool enemyOnTrigger = false;

    [SerializeField]
    private GameObject enemy;

    [SerializeField]
    private GameEvent onEnemyDetected;

    [SerializeField]
    private GameEvent onWait;

    [SerializeField]
    private GameEvent onEnemyNotDetected;




    private void OnTriggerStay(Collider other)
    {

            if (other.tag == "Player")
            {
                Vector3 direccion = other.transform.position - transform.position;

                float angulo = Vector3.Angle(direccion, transform.forward);

                if (-angulo > anguloVision * 0.5f)
                {
                    RaycastHit hit;

                    if (Physics.Raycast(transform.position + transform.up, direccion.normalized, out hit))
                    {
                        Debug.DrawRay(transform.position + transform.up, direccion);
                        if (hit.collider.tag == "Player")
                        {
                        enemyOnTrigger = true;
                        onEnemyDetected.Raise();
                        }
                    }
                }
                else
                {
                    if (enemyOnTrigger)
                    {
                        onWait.Raise();
                    }

                }

            }
        

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enemyOnTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        onEnemyNotDetected.Raise();
    }

}
