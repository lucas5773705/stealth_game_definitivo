using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Finisher_Controller : MonoBehaviour
{

    [SerializeField]
    private GameObject PJ;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Finisher_Demo()
    {
        PJ.transform.forward = gameObject.transform.forward;
        gameObject.transform.position = new Vector3(PJ.transform.position.x + 0.3f, PJ.transform.position.y, PJ.transform.position.z + 0.5f);
    
    }
}
